<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 16.10.20
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello World</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>

<div class="container py-3">
    <div class="row">
        <div class="col-10 mx-auto">
            <div class="accordion" id="faqExample">
                <c:forEach items="${faqList}" var="fq">
                <div class="card">
                    <div class="card-header p-2" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                ${fq.title}
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#faqExample">
                        <div class="card-body">
                            <b>Answer:</b> ${fq.body}
                        </div>
                    </div>
                </div>
                </c:forEach>
<%--                <div class="card">--%>
<%--                    <div class="card-header p-2" id="headingTwo">--%>
<%--                        <h5 class="mb-0">--%>
<%--                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--%>
<%--                                Q: What is Bootstrap 4?--%>
<%--                            </button>--%>
<%--                        </h5>--%>
<%--                    </div>--%>
<%--                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#faqExample">--%>
<%--                        <div class="card-body">--%>
<%--                            Bootstrap is the most popular CSS framework in the world. The latest version released in 2018 is Bootstrap 4. Bootstrap can be used to quickly build responsive websites.--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="card">--%>
<%--                    <div class="card-header p-2" id="headingThree">--%>
<%--                        <h5 class="mb-0">--%>
<%--                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--%>
<%--                                Q. What is another question?--%>
<%--                            </button>--%>
<%--                        </h5>--%>
<%--                    </div>--%>
<%--                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">--%>
<%--                        <div class="card-body">--%>
<%--                            The answer to the question can go here.--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="card">--%>
<%--                    <div class="card-header p-2" id="headingThree">--%>
<%--                        <h5 class="mb-0">--%>
<%--                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--%>
<%--                                Q. What is the next question?--%>
<%--                            </button>--%>
<%--                        </h5>--%>
<%--                    </div>--%>
<%--                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#faqExample">--%>
<%--                        <div class="card-body">--%>
<%--                            The answer to this question can go here. This FAQ example can contain all the Q/A that is needed.--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                </div>--%>
            </div>

        </div>
    </div>
    <!--/row-->
</div>
<!--container-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
