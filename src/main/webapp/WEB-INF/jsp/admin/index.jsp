<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 17.11.19
  Time: 20:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Faq List</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
</head>
<body>
<a href="addFaq">ADD Faq</a><br>

<table id="faqTable">
    <thead>
    <tr>
        <th>Title</th>
        <th>Body</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#faqTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": "getAjaxData"
        } );
    } );
</script>



</body>
</html>
