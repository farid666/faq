package com.faq.Common;

public enum  ColumnType {
    TITLE(0),BODY(1);

    private int value;

    ColumnType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static final ColumnType fromValue(long value) {
        ColumnType columnType = null;
        if (value == 0) {
            columnType = TITLE;
        }else if (value == 1) {
            columnType = BODY;
        }
        return columnType;
    }
}
