package com.faq.service.impl;

import com.faq.domain.DataTableRequest;
import com.faq.domain.DataTableResponse;
import com.faq.domain.Faq;
import com.faq.repository.FaqRepository;
import com.faq.service.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FaqServiceImpl implements FaqService {

    @Autowired
    private FaqRepository faqRepository;

    @Override
    public List<Faq> getListFaq() {
        return faqRepository.getListFaq();
    }

    @Override
    public Faq addFaq(Faq faq) {
        return faqRepository.addFaq(faq);
    }

    @Override
    public void deleteFaq(long id) {
        faqRepository.deleteFaq(id);
    }

    @Override
    public DataTableResponse getDataTableResponse(DataTableRequest request) {
        DataTableResponse response = new DataTableResponse();
        response.setDraw(request.getDraw());
        response.setRecordsTotal(faqRepository.getTotalCountFaq());
        response.setRecordsFiltered(faqRepository.getSearchCount(request.getSearchValue()));
        List<Faq> list = faqRepository.getFaqData(request.getSearchValue(),request.getSortColumn(),request.getSortDirection(),request.getStart(),request.getLength());
        response.setData(new Object[list.size()][3]);

        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                response.getData()[i][0] = list.get(i).getTitle();
                response.getData()[i][1] = list.get(i).getBody();
                response.getData()[i][2] =  "<a class='btn btn-primary' href=\"deleteFaq?id="+list.get(i).getId()+"\">Delete</a>  "+
                        "<a class='btn btn-danger' href=\"editFaq?id="+list.get(i).getId()+"\">Edit</a>";
            }
        }
        return response;
    }

    @Override
    public Optional<Faq> getFaqById(long id) {
        return faqRepository.getFaqById(id);
    }

    @Override
    public void editFaqById(Faq faq) {
        faqRepository.editFaqById(faq);
    }
}
