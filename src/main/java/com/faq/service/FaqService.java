package com.faq.service;

import com.faq.domain.DataTableRequest;
import com.faq.domain.DataTableResponse;
import com.faq.domain.Faq;

import java.util.List;
import java.util.Optional;

public interface FaqService {
    List<Faq> getListFaq();
    Faq addFaq(Faq faq);
    void deleteFaq(long id);
    DataTableResponse getDataTableResponse(DataTableRequest request);
    Optional<Faq> getFaqById(long id);
    void editFaqById(Faq faq);

}
