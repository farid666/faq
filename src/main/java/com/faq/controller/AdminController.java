package com.faq.controller;

import com.faq.Validator.FaqValidator;
import com.faq.domain.DataTableRequest;
import com.faq.domain.DataTableResponse;
import com.faq.domain.Faq;
import com.faq.service.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private FaqService faqService;

    @Autowired
    private FaqValidator faqValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();

        if (target != null) {
            if (target.getClass().equals(Faq.class)) {
                binder.setValidator(faqValidator);
            }
        }
    }

    @GetMapping("/")
    public String index() {
        return "admin/index";
    }

    @GetMapping("/getAjaxData")
    @ResponseBody
    public DataTableResponse getAjaxFaq(
            @RequestParam(name = "draw") long draw,
            @RequestParam(name = "start") long start,
            @RequestParam(name = "length") long length,
            @RequestParam(name = "order[0][column]") long sortColumn,
            @RequestParam(name = "order[0][dir]") String sortDirection,
            @RequestParam(name = "search[value]") String searchValue
    ) {
        DataTableRequest request = new DataTableRequest();
        request.setDraw(draw);
        request.setStart(start);
        request.setLength(length);
        request.setSortColumn(sortColumn);
        request.setSortDirection(sortDirection);
        request.setSearchValue(searchValue);
        return faqService.getDataTableResponse(request);

    }

    @GetMapping("/addFaq")
    public ModelAndView faqPage() {
        ModelAndView modelAndView = new ModelAndView();
        Faq faq = new Faq();
        modelAndView.addObject("faq",faq);
        modelAndView.setViewName("admin/addFaq");
        return modelAndView;
    }

    @PostMapping("/addFaq")
    public String addFaq(@ModelAttribute(name = "faq") @Validated Faq faq,
                               BindingResult result
    ) {
        if (result.hasErrors()) {
            return "admin/addFaq";
        }else {
            faqService.addFaq(faq);
            return "admin/index";
        }

    }

    @GetMapping("/deleteFaq")
    public String deleteFaq(
            @RequestParam(name = "id") long id
    ) {
        try {
            faqService.deleteFaq(id);
            return "admin/index";
        }catch (Exception e) {
            return "errorView";
        }
    }

    @GetMapping("/editFaq")
    public ModelAndView editFaq(@RequestParam(name = "id") long id) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Faq> optionalFaq = faqService.getFaqById(id);
        if (optionalFaq.isPresent()) {
            Faq faq = optionalFaq.get();
            modelAndView.addObject("editFaqs",faq);
            modelAndView.setViewName("admin/editFaq");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @PostMapping("/editFaq")
    public String editFaq2(@ModelAttribute(name = "editFaqs") @Validated Faq faq,
                           BindingResult result){

        if (!result.hasErrors()) {
            faqService.editFaqById(faq);
            return "admin/index";
        }else {
            return "errorView";
        }
    }


}
