package com.faq.Validator;

import com.faq.domain.Faq;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FaqValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Faq.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Faq faq = (Faq) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"title","title required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"body","body required");
    }
}
