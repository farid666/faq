package com.faq.domain;

public class DataTableRequest {
    private long draw;
    private long start;
    private long length;
    private long sortColumn;
    private String sortDirection;
    private String searchValue;

    public DataTableRequest() {
    }

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(long sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    @Override
    public String toString() {
        return "DataTableRequest{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                ", sortColumn=" + sortColumn +
                ", sortDirection='" + sortDirection + '\'' +
                ", searchValue='" + searchValue + '\'' +
                '}';
    }
}
