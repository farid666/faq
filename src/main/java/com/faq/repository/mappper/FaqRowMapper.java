package com.faq.repository.mappper;

import com.faq.domain.Faq;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FaqRowMapper implements RowMapper<Faq> {
    @Override
    public Faq mapRow(ResultSet rs, int i) throws SQLException {
        Faq faq = new Faq();
        faq.setId(rs.getInt("id"));
        faq.setTitle(rs.getString("title"));
        faq.setBody(rs.getString("body"));
        return faq;
    }
}
