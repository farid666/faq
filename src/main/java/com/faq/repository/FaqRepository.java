package com.faq.repository;

import com.faq.domain.DataTableRequest;
import com.faq.domain.DataTableResponse;
import com.faq.domain.Faq;

import java.util.List;
import java.util.Optional;

public interface FaqRepository {
    List<Faq> getListFaq();
    Faq addFaq(Faq faq);
    void deleteFaq(long id);
    long getTotalCountFaq();
    long getSearchCount(String search);
    List<Faq> getFaqData(String search, Long column, String columnDir, Long start, Long length);
    Optional<Faq> getFaqById(long id);
    void editFaqById(Faq faq);
}
