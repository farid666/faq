package com.faq.repository.impl;

public class SqlQuery {
    public static final String GET_FAQ_LIST = "select id,title,body from task_faq ";

    public static final String ADD_FAQ = "insert into task_faq(title,body) " +
            "values(:title,:body) ";

    public static final String DELETE_FAQ = "delete from task_faq where id = :id ";

    public static final String GET_TOTAL_COUNT_FAQ = "select count(id) as count from task_faq ";

    public static final String GET_SEARCH_COUNT = "select count(id) as count from task_faq " +
            "where concat(title,body) like :search ";

    public static final String GET_FAQ_DATA = "select id,title,body from task_faq " +
            "where concat(title,body) like :search " +
            "order by %s %s " +
            "limit %d, %d ";

    public static final String GET_FAQ_BY_ID = "select id,title,body from task_faq " +
            "where id = :id ";

    public static final String UPDATE_FAQ_BY_ID = "update task_faq " +
            "set title = :title, " +
            "body = :body " +
            "where id = :id ";
}
