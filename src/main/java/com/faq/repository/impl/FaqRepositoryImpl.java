package com.faq.repository.impl;

import com.faq.Common.ColumnType;
import com.faq.domain.DataTableRequest;
import com.faq.domain.DataTableResponse;
import com.faq.domain.Faq;
import com.faq.repository.FaqRepository;
import com.faq.repository.mappper.FaqRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.io.StringReader;
import java.util.List;
import java.util.Optional;

@Repository
public class FaqRepositoryImpl implements FaqRepository {


    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private FaqRowMapper faqRowMapper;

    @Override
    public List<Faq> getListFaq() {
        return jdbcTemplate.query(SqlQuery.GET_FAQ_LIST,new MapSqlParameterSource(),faqRowMapper);
    }

    @Override
    public Faq addFaq(Faq faq) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("title",faq.getTitle());
        params.addValue("body",faq.getBody());
        int count = jdbcTemplate.update(SqlQuery.ADD_FAQ,params);
        if (count == 1) {
            System.out.println("added faq");
        }else {
            throw new RuntimeException("can not add faq");
        }
        return faq;
    }

    @Override
    public void deleteFaq(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        int count = jdbcTemplate.update(SqlQuery.DELETE_FAQ,params);
        if (count == 1) {
            System.out.println("Faq silindi");
        }else {
            throw new RuntimeException("silinmedi");
        }
    }

    @Override
    public long getTotalCountFaq() {
        return jdbcTemplate.queryForObject(SqlQuery.GET_TOTAL_COUNT_FAQ,new MapSqlParameterSource(),Long.class);
    }

    @Override
    public long getSearchCount(String search) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%" +search+ "%");
        return jdbcTemplate.queryForObject(SqlQuery.GET_SEARCH_COUNT,params,Long.class);
    }

    @Override
    public List<Faq> getFaqData(String search, Long column, String columnDir, Long start, Long length) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%" +search+"%");
        String sql = String.format(SqlQuery.GET_FAQ_DATA, ColumnType.fromValue(column),columnDir,start,length);
        return jdbcTemplate.query(sql,params,faqRowMapper);
    }

    @Override
    public Optional<Faq> getFaqById(long id) {
        Optional<Faq> optionalFaq = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id",id);
        List<Faq> list = jdbcTemplate.query(SqlQuery.GET_FAQ_BY_ID,params,faqRowMapper);
        if (!list.isEmpty()) {
            optionalFaq = Optional.of(list.get(0));
        }
        return optionalFaq;
    }

    @Override
    public void editFaqById(Faq faq) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("title",faq.getTitle());
        params.addValue("body",faq.getBody());
        params.addValue("id",faq.getId());
        int count = jdbcTemplate.update(SqlQuery.UPDATE_FAQ_BY_ID,params);
        if (count == 1) {
            System.out.println("edit Faq");
        }else {
            throw new RuntimeException("can not edit faq");
        }
    }

}
